# N13T's Helm Charts

## TL;DR;
```
helm repo add n13t https://charts.n13t.dev/
```

## Incubator charts
```
helm repo add incubator https://charts.n13t.dev/incubator
```

## Included charts
+ lemp (NGINX + PHP)
+ auto-deploy-app (gitlab's chart for AutoDevOps CI)

## Who use this?
- Our team at [VDATLab.com](https://www.vdatlab.com)
- Me (definitely!)

## Target goals
* Follow Stable charts at https://github.com/helm/charts
* Optimize for High Availability deployment
* Minimal change & maintain approach compare to Stable charts
* Avoid [ReadWriteMany](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#access-modes) access mode. 

## Status of the Project
Under active development
