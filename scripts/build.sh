HELM_IMAGE=alpine/helm:3.0.2

## for update copy the script in below to GitLab CI
echo '
  ## Packaging primary repo (n13t)
  helm lint n13t/*
  for chart in n13t/*; do helm dep up $chart; done
  mkdir -p public && cd public && helm package ../n13t/* && helm repo index . --merge index.yaml --url $CI_PAGES_URL && cd ..

  ## Packaging other repos
  HELM_REPOS="incubator"
  for repo in $HELM_REPOS; do
    helm lint $repo/*
    for chart in $repo/*; do helm dep up $chart; done
    mkdir -p public/$repo && cd public/$repo && helm package ../../$repo/* && cd ../../
  done

  ## Pulling charts from other repositories
  helm repo add stable https://kubernetes-charts.storage.googleapis.com/
  helm repo add bitnami https://charts.bitnami.com/bitnami
  helm repo update
  while read chart; do
    chart=`echo $chart | cut -d " " -f1`
    version=`echo $chart | cut -d ":" -f2`
    chart=`echo $chart | cut -d ":" -f1`
    repo=`echo $chart | cut -d "/" -f1`
    mkdir -p public/$repo
    printf "helm pull %s --version %s\n" $chart $version
    eval $(printf "helm pull %s --version %s\n -d public/$repo" $chart $version)
  done < CHART_TO_FETCH

  ## Create index.yaml
  cd public/
  for repo in */; do
    helm repo index $repo --merge $repo/index.yaml --url $CI_PAGES_URL/$repo
  done
  cd ..
' > /tmp/build-charts.sh
chmod +x /tmp/build-charts.sh

docker run --rm -it -v /tmp/build-charts.sh:/tmp/build-charts.sh -v $PWD:/apps -e CI_PAGES_URL=http://looalhost --entrypoint /bin/sh $HELM_IMAGE /tmp/build-charts.sh
sudo chown $USER -R .
